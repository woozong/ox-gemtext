;;; ox-gemtext.el --- Output gemtext formatted documents from org-mode  -*- lexical-binding: t; -*-

;; Author: Geert Vermeiren <geertv@surfspot.be>
;; URL:
;; Keywords: org gemini gemtext publish
;; Version: 0.1
;; Package-Requires: ((emacs "26.1"))
;; SPDX-License-Identifier: GPL-3.0-or-later


;;; Commentary:
;;
;; This package provides an org-mode export backend for the
;; gemtext format, the default text-based format used by the
;; gemini protocol.

(require 'ox)
(require 'ox-publish)
(require 'ox-ascii)
(require 'cl-lib)

(eval-when-compile
  (progn
    (checkdoc)))

;;; Code:
(org-export-define-derived-backend 'gemtext 'ascii
  :menu-entry '(?g "Export to Gemini"
                   ((?b "To buffer"
	                (lambda (a s v b)
	                  (org-gemtext-export-to-buffer a s v b nil)))
	            (?f "To file"
	                (lambda (a s v b)
	                  (org-gemtext-export-to-file a s v b nil)))))

  :translate-alist '((bold . org-ascii-bold)
                     (center-block . org-ascii-center-block)
                     (clock . org-ascii-clock)
                     (code . org-ascii-code)
                     (drawer . org-ascii-drawer)
                     (dynamic-block . org-ascii-dynamic-block)
                     (entity . org-ascii-entity)
                     (example-block . org-ascii-example-block)
                     (export-block . org-ascii-export-block)
                     (export-snippet . org-ascii-export-snippet)
                     (fixed-width . org-ascii-fixed-width)
                     (footnote-reference . org-ascii-footnote-reference)
                     (headline . org-ascii-headline)
                     (horizontal-rule . org-ascii-horizontal-rule)
                     (inline-src-block . org-ascii-inline-src-block)
                     (inlinetask . org-ascii-inlinetask)
                     (inner-template . org-ascii-inner-template)
                     (italic . org-ascii-italic)
                     (item . org-ascii-item)
                     (keyword . org-ascii-keyword)
                     (latex-environment . org-ascii-latex-environment)
                     (latex-fragment . org-ascii-latex-fragment)
                     (line-break . org-ascii-line-break)
                     (link . org-ascii-link)
                     (node-property . org-ascii-node-property)
                     (paragraph . org-ascii-paragraph)
                     (plain-list . org-ascii-plain-list)
                     (plain-text . org-ascii-plain-text)
                     (planning . org-ascii-planning)
                     (property-drawer . org-ascii-property-drawer)
                     (quote-block . org-ascii-quote-block)
                     (radio-target . org-ascii-radio-target)
                     (section . org-ascii-section)
                     (special-block . org-ascii-special-block)
                     (src-block . org-ascii-src-block)
                     (statistics-cookie . org-ascii-statistics-cookie)
                     (strike-through . org-ascii-strike-through)
                     (subscript . org-ascii-subscript)
                     (superscript . org-ascii-superscript)
                     (table . org-ascii-table)
                     (table-cell . org-ascii-table-cell)
                     (table-row . org-ascii-table-row)
                     (target . org-ascii-target)
                     (template . org-ascii-template)
                     (timestamp . org-ascii-timestamp)
                     (underline . org-ascii-underline)
                     (verbatim . org-ascii-verbatim)
                     (verse-block . org-ascii-verse-block)))

(defun org-gemtext-export-to-buffer (&optional async subtreep visible-only body-only ext-plist)
  "Export an org file to a new buffer.

A non-nil optional argument ASYNC means the process should happen
asynchronously.  The resulting buffer should be accessible
through the `org-export-stack' interface.

When optional argument SUBTREEP is non-nil, export the sub-tree
at point, extracting information from the headline properties
first.

When optional argument VISIBLE-ONLY is non-nil, don't export
contents of hidden elements.

When optional argument BODY-ONLY is non-nil, strip title and
table of contents from output.

EXT-PLIST, when provided, is a property list with external
parameters overriding Org default settings, but still inferior to
file-local settings."
  (interactive)
  (org-export-to-buffer 'gemtext "*Org Gemtext Export*" async subtreep visible-only body-only ext-plist (lambda () (text-mode))))


(defun org-gemtext-export-to-file (&optional async subtreep visible-only body-only ext-plist)
  "Export an org file to a gemini file.

A non-nil optional argument ASYNC means the process should happen
asynchronously.  The resulting buffer should be accessible
through the `org-export-stack' interface.

When optional argument SUBTREEP is non-nil, export the sub-tree
at point, extracting information from the headline properties
first.

When optional argument VISIBLE-ONLY is non-nil, don't export
contents of hidden elements.

When optional argument BODY-ONLY is non-nil, strip title and
table of contents from output.

EXT-PLIST, when provided, is a property list with external
parameters overriding Org default settings, but still inferior to
file-local settings."
  (interactive)
  (let ((file (org-export-output-file-name ".gmi" subtreep)))
    (org-export-to-file 'gemtext file
      async subtreep visible-only body-only ext-plist)))

(defun org-gemtext-publish-to-gemtext (plist filename pub-dir)
  "Publish an org file to a gemtext file.

FILENAME is the filename of the Org file to be published.  PLIST
is the property list for the given project.  PUB-DIR is the
publishing directory.

Return output file name."
  (org-publish-org-to
   'gemtext filename ".gmi" plist pub-dir))

(provide 'ox-gemtext)
;;; ox-gemtext.el ends here
