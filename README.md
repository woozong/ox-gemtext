# ox-gemtext

This package provides an org-mode export back-end for the gemtext format, the default text-based format used by the gemini protocol.